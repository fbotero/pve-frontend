//Tabla de calculos intermedios en el que se adicionan todas las propiedades necesarias para la inserción de los datos en la misma, dicha tabla es para crear el nombre de los campos
// Que conformarán dicha tabla, no para hacer algún proceso contable.
import React from 'react';
import MaterialTable from 'material-table';
import {Container} from "@material-ui/core";

export default function TablaIntermedios(props) {
    const [state, setState] = React.useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Infraestructura',
                field: 'infraMaquinas',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Mobiliario',
                field: 'mobiliario',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Equipos informáticos',
                field: 'equiposInformaticos',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Vehículos',
                field: 'vehiculos',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Otros',
                field: 'otros',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total no corriente',
                field: 'totalNoCorriente',
                editable: 'never',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
            {
                title: 'Existencias iniciales',
                field: 'existenciasIniciales',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Liquidez & Tesorería',
                field: 'liquidez',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total corriente',
                field: 'totalCorriente',
                editable: 'never',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
            {
                title: 'Total inversion',
                field: 'totalInversion',
                editable: 'never',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
        ],
        data: props.data,
    });

    return (
        <Container>
            <MaterialTable
                title={props.title}
                columns={state.columns}
                data={state.data}
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
            />
        </Container>
    );
}
