import firebase from "firebase";

let firebaseConf = {
    apiKey: "AIzaSyCpEcOuuoCOeRJ3y2R9bGUeX_AW6wUeYR0",
    authDomain: "pve-autenticacion.firebaseapp.com",
    databaseURL: "https://pve-autenticacion.firebaseio.com",
    projectId: "pve-autenticacion",
    storageBucket: "pve-autenticacion.appspot.com",
    messagingSenderId: "467846400640",
    appId: "1:467846400640:web:e5fb9d331008918e967465",
    measurementId: "G-SGB1LRPQKJ"
};

firebase.initializeApp(firebaseConf);
firebase.firestore();

export default firebase;
