export let resultadoMoment = [
    {
        tiempo: 'Datos',
        codigoProyecto: '',
        ventas: 0,       
        aprovisionamiento: 0,
        variacionexistencias: 0,
        margen: 0, 
        gastospersonal: 0,
        alquileres: 0,
        otrosgastos: 0,
        EBITDA: 0,
        amortizacion: 0,
        EBIT: 0,
        gastosfinancieros: 0,
        BAI: 0,
        impuestobeneficios: 0,
        resultado: 0,
    },
    {
        tiempo: 'Año 1',
        codigoProyecto: '',
        ventas: 0,
        aprovisionamiento: 0,
        variacionexistencias: 0,
        margen: 0, 
        gastospersonal: 0,
        alquileres: 0,
        otrosgastos: 0,
        EBITDA: 0,
        amortizacion: 0,
        EBIT: 0,
        gastosfinancieros: 0,
        BAI: 0,
        impuestobeneficios: 0,
        resultado: 0,
    },
    {
        tiempo: 'Año 2',
        codigoProyecto: '',
        ventas: 0,
        aprovisionamiento: 0,
        variacionexistencias: 0,
        margen: 0,
        gastospersonal: 0,
        alquileres: 0,
        otrosgastos: 0,
        EBITDA: 0,
        amortizacion: 0,
        EBIT: 0,
        gastosfinancieros: 0,
        BAI: 0,
        impuestobeneficios: 0,
        resultado: 0,
    },
    {
        tiempo: 'Año 3',
        codigoProyecto: '',
        ventas: 0,
        aprovisionamiento: 0,
        variacionexistencias: 0,
        margen: 0, 
        gastospersonal: 0,
        alquileres: 0,
        otrosgastos: 0,
        EBITDA: 0,
        amortizacion: 0,
        EBIT: 0,
        gastosfinancieros: 0,
        BAI: 0,
        impuestobeneficios: 0,
        resultado: 0,     
    },
    {
        tiempo: 'Año 4',
        codigoProyecto: '',
        ventas: 0,
        aprovisionamiento: 0,
        variacionexistencias: 0,
        margen: 0,
        gastospersonal: 0,
        alquileres: 0,
        otrosgastos: 0,
        EBITDA: 0,
        amortizacion: 0,
        EBIT: 0,
        gastosfinancieros: 0,
        BAI: 0,
        impuestobeneficios: 0,
        resultado: 0,
      },
    {
        tiempo: 'Año 5',
        codigoProyecto: '',
        ventas: 0,
        aprovisionamiento: 0,
        variacionexistencias: 0,
        margen: 0,
        gastospersonal: 0,
        alquileres: 0,
        otrosgastos: 0,
        EBITDA: 0,
        amortizacion: 0,
        EBIT: 0,
        gastosfinancieros: 0,
        BAI: 0,
        impuestobeneficios: 0,
        resultado: 0, 
    },
];
