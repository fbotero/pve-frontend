//Rutas en el que estarán los distintos procesos del aplicativo para ejecutar la acción deseada por el usuario
import React from 'react';
import './App.css';
import NavBar from "../shared/navbar/NavBar";
import {
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import StepperComponent from "../clear_plan/crear_plan";
import Home from "../home/Home";
import Login from "../usuario/login";
import Footer from "../shared/footer/Footer";
import Registro from "../usuario/registro";
import Ayuda from "../shared/ayuda/ayuda";
import ListaProyectos from "../proyectos/lista_proyectos";
import FormularioProyecto from "../proyectos/crear_proyecto";
import EditarProyecto from "../proyectos/editar_proyecto";

function App() {
    return (
        <div className="App">
            <Router>
                <NavBar/>
                <Route path='/' exact component={Home}/>
                <Route path='/ayuda' component={Ayuda}/>
                <Route path='/login' component={Login}/>
                <Route path='/registro' component={Registro}/>
                <Route path='/crear-plan/:codigoProyecto' component={StepperComponent}/>
                <Route path='/proyectos' component={ListaProyectos}/>
                <Route path='/crear-proyecto' component={FormularioProyecto}/>
                <Route path='/editar-proyecto/:codProyecto' component={EditarProyecto}/>

            </Router>
            <br/>
            <br/>
            <Footer/>
            <br/>
        </div>
    );
}

export default App;
