//Componente encargada de la información del usuario, la agregación del mismo y el inicio el proceso del plan viable con el paso de inversiones
import {constants} from '../constants/action-types';

export const agregarInformacionUsuario = (idUsuario) => {
    return (dispatch) => {
        dispatch({type: constants.ADD_USER_DATA, data: idUsuario});
    }
};

export const inversionesEstado = (inversiones) => {
    return (dispatch) => {
        dispatch({type: constants.ADD_INVERSION_DATA, data: inversiones});
    }
};
