import React, {useState} from "react";
import {connect} from "react-redux";
import {makeStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import {Redirect} from "react-router-dom";
import {constants} from "../../constants/action-types";
import axios from "axios";
import {toast} from "react-toastify";
import { useStyles } from './estilosProyecto';

const FormularioProyecto = (props) => {

    const classes = useStyles();

    const {codProyecto} = props.match.params;

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    const [redirect, setRedirect] = useState(false);
    const [id, setId] = useState(0);
    const [codigoProyecto, setCodigoProyecto] = useState("");
    const [nombre, setNombre] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const [logo, setLogo] = useState("https://uci.ac.cr/wp-content/uploads/2017/03/ffee2447b152494b43d9816faaea83c8_XL.jpg");
    const [fecha_creacion, setFecha_creacion] = useState(null);
    const [idUsuario, setIdUsuario] = useState(props.auth.uid);

    if (codProyecto) {
        console.log('codig edit', codProyecto);
        axios.get(`${constants.URL_API}/proyecto/${codProyecto}`)
            .then(proyecto => {
                let data = proyecto.data;
                console.log('Proy a editar ', data);
                setId(data.id);
                setCodigoProyecto(data.codigoProyecto);
                setNombre(data.nombre);
                setDescripcion(data.descripcion);
                setLogo(data.logo);
                setFecha_creacion(data.fecha_creacion);
                setIdUsuario(data.idUsuario);
            })
    }

    const guardarProyecto = async (event) => {
        event.preventDefault();
        console.log('proy');
        const proyecto = {
            id,
            codigoProyecto,
            nombre,
            descripcion,
            logo,
            fecha_creacion,
            idUsuario
        };

        await axios.post(`${constants.URL_API}/proyectos/guardar`, proyecto)
            .then(() => {
                notify("Proyecto creado exitosamente");
                setRedirect(true)
            }).catch(error => {
                console.log('error', error);
                notify("Ha ocurrido un error creando el proyecto");
            });
    };

    if (!props.auth.uid) return <Redirect to={'/login'}/>;

    if (redirect) return <Redirect to={'/proyectos'}/>;

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Registra tu proyecto
                </Typography>
                <form className={classes.form} noValidate={false} onSubmit={guardarProyecto}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        type="text"
                        fullWidth
                        defaultValue={nombre}
                        onChange={(e) => setNombre(e.target.value)}
                        id="nombre"
                        label="Nombre del proyecto"
                        name="nombre"
                        autoComplete="nombre"
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        defaultValue={descripcion}
                        onChange={(e) => setDescripcion(e.target.value)}
                        multiline
                        rows={3}
                        name="descripcion"
                        label="Descripcion"
                        type="textarea"
                        id="descripcion"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Guardar proyecto
                    </Button>
                </form>
            </div>
        </Container>
    );
};
const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};

export default connect(mapStateToProps)(FormularioProyecto)
