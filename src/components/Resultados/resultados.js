import React, { useEffect, useState } from 'react';
import MaterialTable from 'material-table';
import { Container } from "@material-ui/core";
import { resultadoMoment } from "../../constants/ResultadosActualData.js";
import axios from "axios";
import { constants } from "../../constants/action-types";
import Button from "@material-ui/core/Button";
import { toast } from "react-toastify";

const numero2 = 12;
const numero1 = 1; 

function Resultados(props) {

    const [Resultadoss, setResultadoss] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    let { codigoProyecto } = props;

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    useEffect(() => {
        const getResultados = async () => {
            const response = await axios.get(`${constants.URL_API}/Resultados/${codigoProyecto}`);
            let data = response.data;
            if (data.length > 0) {
                setState(prevState => {
                    return { ...prevState, data }
                });
            } else {
                setState(prevState => {
                    return { ...prevState, resultadoMoment }
                });
            }

        };
        getResultados();
    }, []);

    const guardarResultados = async () => {
        console.log('Resultados a guardar ', Resultadoss);
        await axios.post(`${constants.URL_API}/Resultados/guardar`, Resultadoss)
            .then(() => {
                notify("Se ha guardado tu avance");
            }).catch(error => {
                console.log('error: ', error)
            })
    };

    const hacerCalculos = (Resultados) => {
        Resultados.forEach(value => {
            
            value.salarioMensual2 = parseFloat(value.salarioMensual) * numero2 * parseFloat(value.numeroEmpleadosAno1) ;
            value.alquilerMensual2 = parseFloat(value.alquilerMensual) * numero2 ;
            value.electricidad2 = parseFloat(value.electricidad) * numero2;
            value.telefono2 = parseFloat(value.telefono) * numero2;
            value.materialOficina2 = parseFloat(value.materialOficina) * numero2;
            value.limpieza2 = parseFloat(value.limpieza) * numero2;
            value.totalOtrosResultados = parseFloat(value.electricidad2) + parseFloat(value.telefono2) + parseFloat(value.materialOficina2)+ parseFloat(value.limpieza2);

            
            value.codigoProyecto = codigoProyecto
        });
        setResultadoss(Resultados);
    };

    const [state, setState] = useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Ventas',
                field: 'ventas',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Aprovisionamiento',
                field: 'aprovisionamiento',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Variacion De Existencias',
                field: 'variacionexistencias',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Margen',
                field: 'margen',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Gastos De Personal',
                field: 'gastospersonal',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Alquileres',
                field: 'alquileres',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Otros Gastos',
                field: 'otrosgastos',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'EBITDA',
                field: 'EBITDA',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Amortizaciones',
                field: 'amortizacion',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'EBIT',
                field: 'EBIT',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Gastos Financieros',
                field: 'gastosfinancieros',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'BAI',
                field: 'BAI',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Impuesto Sobre Beneficios',
                field: 'impuestobeneficios',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'resultado',
                field: 'resultado',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
        ],
        data: Resultadoss.length > 0 ? Resultadoss : resultadoMoment
    });

    console.log('dabt', Resultadoss);

    return (
        <Container>
            <MaterialTable
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                title="Cuenta De Resultados"
                columns={state.columns}
                data={state.data}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        hacerCalculos(data);
                                        return { ...prevState, data };
                                    });
                                }
                            }, 600);
                        }),
                }}
            />
            <br />
            <Button onClick={guardarResultados} variant="outlined" color="primary">
                Guardar Avance
            </Button>
        </Container>
    );
}

export default Resultados