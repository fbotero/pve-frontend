//Proceso de acceso, registro y salida del aplicativo, el proceso es similar en cada uno de las opciones y es necesario de un cortafuegos para que sea más segura el almacenamiento de datos.
import {constants} from '../../constants/action-types';

export const signIn = (credenciales) => {
    console.log(credenciales);
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();
        firebase.auth().signInWithEmailAndPassword(
            credenciales.email,
            credenciales.password
        ).then((resp) => {
            dispatch({type: constants.LOGIN_OK});
        }).catch((err) => {
            dispatch({type: constants.LOGIN_ERROR, err})
        })
    }
};

export const signOut = () => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();
        firebase.auth().signOut().then(() => {
            dispatch({type: constants.SIGNOUT_SUCCESS});
            dispatch({type: 'RESET_STATE'})
        }).catch((err) => {
            dispatch({type: constants.SIGNOUT_ERROR, err})
        })
    }
};

export const signUp = (usuario) => {
    return (dispatch, getState, {getFirebase, getFirestore}) => {
        const firebase = getFirebase();
        const firestore = getFirestore();
        firebase.auth().createUserWithEmailAndPassword(
            usuario.correo,
            usuario.contrasenia
        ).then(resp => {
            return firestore.collection('usuarios')
                .doc(resp.user.uid)
                .set({
                    nombres: usuario.nombres,
                    apellidos: usuario.apellidos,
                    iniciales: usuario.nombres[0] + usuario.apellidos[0]
                })
        }).then(() => {
            dispatch({type: constants.SIGNUP_SUCCESS})
        }).catch((err) => {
            dispatch({type: constants.SIGNUP_ERROR, err})
        })
    }
};
