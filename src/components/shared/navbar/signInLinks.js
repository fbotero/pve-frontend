import React from 'react'
import {Link, NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {signOut} from '../../../actions/auth/authActions';
import Button from "@material-ui/core/Button";

const SignedInLinks = (props) => {

    console.log('Perfil ', props.profile);

    return (
        <div>
            <Button component={Link} to={'/proyectos'} color="inherit">Proyectos</Button>
            <Button onClick={props.signOut} color="inherit">Salir</Button>
            <NavLink to={'/'} style={{ textDecoration: 'none', backgroundColor: '#A4A4A4', color: 'white', borderRadius: '100%', padding: 10}}>
                {props.profile.iniciales}
            </NavLink>
        </div>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
};

export default connect(null, mapDispatchToProps)(SignedInLinks)
