//Clase principal para la adición de esquemas del plan viable de emprendimiento,en esta clase es donde se adicionarán los pasos generales a desarrollar para poderlo mostrar en la página
//Principal en la opción de proyectos.
import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Inversiones from "../inversiones/inversiones";
import {agregarInformacionUsuario} from '../../actions/pveActions'
import CalculosIntermedios from '../calculos_intermedios/calculos_intermedios';
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import Presupuestos from '../presupuesto/presupuesto';
import Financiacion from "../financiacion/financiacion";
import Gastos from '../Gastos/gastos';
import VentasCompras from '../ventasycompras/ventasycompras';
import Resultados from '../Resultados/resultados';

const useStyles = makeStyles(theme => ({
    botones: {
        textAlign: 'center',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    root: {
        width: '100%',
    },
}));

const StepperComponent = (props) => {
    const {auth} = props;

    const { codigoProyecto } = props.match.params;

    let getSteps = () => {
        return ['Inversiones', 'Presupuesto', 'Financiación','Ventas Y Compras', 'Gastos', 'Resultados'];
    };

    let getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <Inversiones codigoProyecto={codigoProyecto}/>;
            case 1:
                return <Presupuestos codigoProyecto={codigoProyecto}/>;
            case 2:
                return <Financiacion codigoProyecto={codigoProyecto}/>;
            case 3:
                return <VentasCompras codigoProyecto={codigoProyecto}/>;
            case 4:
                return <Gastos/>;
            case 5:
                return <Resultados/>;
            default:
                return 'No hay información';
        }
    };

    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        props.agregarInfoUser(auth.uid);
        console.log(activeStep);
    };


    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    if (!auth.uid) return <Redirect to={'/login'}/>;

    return (
        <div className={classes.root}>
            <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map(label => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                ))}
            </Stepper>
            <div>
                <div className="container">
                    <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                    <br/>
                    <br/>
                    <div className={classes.botones}>
                        <Button
                            color="primary"
                            variant="contained"
                            disabled={activeStep === 0}
                            onClick={handleBack}
                            className={classes.backButton}
                        >
                            Atras
                        </Button>
                        <Button variant="contained" color="primary" onClick={handleNext}
                                disabled={activeStep === steps.length - 1}>
                            Siguiente
                        </Button>
                        <br/>
                    </div>
                    <br/>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        agregarInfoUser: (idUsuario) => dispatch(agregarInformacionUsuario(idUsuario)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(StepperComponent)
