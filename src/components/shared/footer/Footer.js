import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import React from "react";

function Footer() {
    return (
        <Typography variant="body2" color="textSecondary" align="center" position='fixed'>
            {'Copyright © '}
            <Link color="inherit" href="#">
                Corporación Universitaria Adventista
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default Footer;
