//Unión de la parte de calculos intermedios con inversiones y los calculos contables del mismo
import React, {useEffect, useState} from 'react';
import TablaIntermedios from '../calculos_intermedios/tabla_intermedios';
import {intermedioData} from "../../constants/intermedioActualData";

export default function CalculosIntermedios() {
    const [dataActivosNoCorriente, setDataActivosNoCorriente] = useState(intermedioData);
    const [dataDotacionAmortizaciones, setDataDotacionAmortizaciones] = useState(intermedioData);
    const [dataAmortizaionAcomulada, setDataAmortizaionAcomulada] = useState(intermedioData);

    useEffect(() => {
        //Obtener calculos intermedios
    });

    return (
        <div>
            <TablaIntermedios data={dataActivosNoCorriente} title={'Total Activos No Corrientes'}/>
            <br/>
            <TablaIntermedios data={dataDotacionAmortizaciones} title={'Dotación Amortizaciones'}/>
            <br/>
            <TablaIntermedios data={dataAmortizaionAcomulada} title={'Amortizacion Acomulada'}/>
        </div>

    )
}
