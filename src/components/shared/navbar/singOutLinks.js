import React from 'react'
import {Link} from 'react-router-dom'
import Button from "@material-ui/core/Button";

const SignedOutLinks = () => {
    return (
        <div className='right-aligned'>
            <Button component={Link} to={'/login'} color="inherit">Login</Button>
            <Button component={Link} to={'/registro'} color="inherit">Registro</Button>
        </div>
    )
};

export default SignedOutLinks;
