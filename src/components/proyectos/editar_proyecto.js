import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import {Redirect} from "react-router-dom";
import {constants} from "../../constants/action-types";
import axios from "axios";
import {useStyles} from './estilosProyecto.js';

const EditarProyecto = (props) => {

    const classes = useStyles();

    const {codProyecto} = props.match.params;

    const [redirect, setRedirect] = useState(false);

    const [proyecto, setProyecto] = useState({
        nombre: '',
        descripcion: ''
    });


    useEffect(() => {
        axios.get(`${constants.URL_API}/proyecto/${codProyecto}`)
            .then(proyec => {
                setProyecto(proyec.data);
            })
    }, []);

    console.log('Proy a editar ', proyecto);

    const guardarProyecto = async (event) => {
        event.preventDefault();
        console.log('proy');

        await axios.post(`${constants.URL_API}/proyectos/guardar`, proyecto)
            .then(() => {
                setRedirect(true)
            }).catch(error => {
                console.log('error', error);
            });
    };

    if (!props.auth.uid) return <Redirect to={'/login'}/>;

    if (redirect) return <Redirect to={'/proyectos'}/>;

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Editando proyecto
                </Typography>
                <form className={classes.form} noValidate={false} onSubmit={guardarProyecto}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        type="text"
                        fullWidth
                        value={proyecto.nombre}
                        onChange={(e) => setProyecto({...proyecto, nombre: e.target.value})}
                        id="nombre"
                        label="Nombre del proyecto"
                        name="nombre"
                        autoComplete="nombre"
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        value={proyecto.descripcion}
                        onChange={(e) => setProyecto({...proyecto, descripcion: e.target.value})}
                        multiline
                        rows={3}
                        name="descripcion"
                        label="Descripcion"
                        type="textarea"
                        id="descripcion"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Editar proyecto
                    </Button>
                </form>
            </div>
        </Container>
    );
};
const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};

export default connect(mapStateToProps)(EditarProyecto)
