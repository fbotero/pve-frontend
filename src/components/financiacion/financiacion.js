// Procesos contables del paso de financiación
import React, {useEffect, useState} from "react";
import axios from "axios";
import {constants} from "../../constants/action-types";
import {financiacionMoment} from "../../constants/financiacionActual";
import {Container} from "@material-ui/core";
import MaterialTable from "material-table";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import {makeStyles} from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import {FinanciacionCalculo} from "./financiacionCalculos";
import {connect} from "react-redux";


const useStyles = makeStyles((theme) => ({
    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

function Financiacion(props) {
    const [financiations, setfinanciations] = useState([]);
    const [capitalVivo, setCapitalVivo] = useState([]);
    const [gastosFiancieros, setGastosFiancieros] = useState([]);
    const [devolucionPrestamos, setDevolucionPrestamos] = useState([]);
    let [verCalculos, setVerCalculos] = useState(false);
    const mensajeCalculos1 = 'Ver cálculos';
    const mensajeCalculos2 = 'Ocultar cálculos';
    let {codigoProyecto, inversiones} = props;
    const classes = useStyles();

    console.log('inver ', inversiones);

    useEffect(() => {
        const getFinanciaciones = async () => {
            const response = await axios.get(`${constants.URL_API}/financiacion/${codigoProyecto}`);
            let {financiacion, capitalVivo, gastosFinancieros, devolucionPrestamos} = response.data;
            setCapitalVivo(capitalVivo);
            setGastosFiancieros(gastosFinancieros);
            setDevolucionPrestamos(devolucionPrestamos);
            financiacion.forEach((data, i) => {
                data.inversion = inversiones[i].totalInversion;
            });
            if (financiacion.length > 0) {
                setState(prevState => {
                    return {...prevState, data: financiacion}
                });
            } else {
                setState(prevState => {
                    return {...prevState, financiacionMoment}
                });
            }

        };
        getFinanciaciones();
    }, []);

    const [state, setState] = useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'PPTO emprendor',
                field: 'pptoEmprendedor',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamos',
                field: 'prestamos',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Tipo de interés',
                field: 'tipoInteres',
                sorting: false,
                type: 'percent',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Años',
                field: 'anios',
                sorting: false,
                type: 'numeric',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total financiacion',
                field: 'totalFinaciacion',
                sorting: false,
                type: 'currency',
                editable: 'never',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Inversion',
                field: 'inversion',
                sorting: false,
                type: 'currency',
                editable: 'never',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
            {
                title: 'Financiacion',
                field: 'financiacion',
                sorting: false,
                type: 'currency',
                editable: 'never',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            }
        ],
        data: financiations.length > 0 ? financiations : financiacionMoment
    });

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    const guardarFianciacion = async () => {
        let data = {
            financiacion: financiations,
            capitalVivo: capitalVivo,
            gastosFinancieros: gastosFiancieros,
            devolucionPrestamos: devolucionPrestamos
        };
        console.log('Data financiaciones a guardar ', data);
        await axios.post(`${constants.URL_API}/financiacion/guardar`, data)
            .then(() => {
                notify("Se ha guardado tu avance");
            }).catch(error => {
                console.log('error: ', error)
            });
    };

    function calcularTotalPrestamo(data) {
        data.forEach((financiacion, i) => {
            financiacion.totalFinaciacion = parseFloat(financiacion.pptoEmprendedor) + parseFloat(financiacion.prestamos);
            financiacion.codigoProyecto = codigoProyecto;
            financiacion.financiacion = financiacion.totalFinaciacion;
            financiacion.inversion = inversiones[i].totalInversion;
        });
        setfinanciations(data);
    }

    function calculosFinanciacionCapitalVivo(data) {
        console.log('capital activo ', data);
        data.forEach(capitalActivo => {
            capitalActivo.totalPrestamo = parseFloat(capitalActivo.prestamo0) + parseFloat(capitalActivo.prestamo1) +
                parseFloat(capitalActivo.prestamo2) + parseFloat(capitalActivo.prestamo3) +
                parseFloat(capitalActivo.prestamo4) + parseFloat(capitalActivo.prestamo5);
            capitalActivo.codigoProyecto = codigoProyecto;
        });
        setCapitalVivo(data)
    }

    function calculosFinanciacionGastosFinancieros(data) {
        console.log('gastos ', data);
        data.forEach(gastoFinanciero => {
            gastoFinanciero.totalPrestamo = parseFloat(gastoFinanciero.prestamo0) + parseFloat(gastoFinanciero.prestamo1) +
                parseFloat(gastoFinanciero.prestamo2) + parseFloat(gastoFinanciero.prestamo3) +
                parseFloat(gastoFinanciero.prestamo4) + parseFloat(gastoFinanciero.prestamo5);
            gastoFinanciero.codigoProyecto = codigoProyecto;
        });
        setGastosFiancieros(data)
    }

    function calculosFinanciacionDevolucionPrestamos(data) {
        console.log('dev prestamos ', data);
        data.forEach(devolicionPrestamo => {
            devolicionPrestamo.totalPrestamo = parseFloat(devolicionPrestamo.prestamo0) + parseFloat(devolicionPrestamo.prestamo1) +
                parseFloat(devolicionPrestamo.prestamo2) + parseFloat(devolicionPrestamo.prestamo3) +
                parseFloat(devolicionPrestamo.prestamo4) + parseFloat(devolicionPrestamo.prestamo5);
            devolicionPrestamo.codigoProyecto = codigoProyecto;
        });
        setDevolucionPrestamos(data)
    }

    function verOcultarCalculos() {
        setVerCalculos(!verCalculos);
    }

    return (
        <Container>
            <MaterialTable
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                title="Fianciacion"
                columns={state.columns}
                data={state.data}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        calcularTotalPrestamo(data);
                                        return {...prevState, data};
                                    });
                                }
                            }, 600);
                        }),
                }}
            />
            <br/>
            <div align={'center'} style={{paddingTop: 10}}>
                <IconButton onClick={verOcultarCalculos} aria-label="delete" className={classes.margin} size="small">
                    {!verCalculos ? mensajeCalculos1 : mensajeCalculos2} {!verCalculos ?
                    <ArrowDownwardIcon fontSize="inherit"/> : <ArrowUpwardIcon fontSize="inherit"/>}
                </IconButton>
            </div>
            <br/>
            {
                verCalculos ?
                    <div>
                        <FinanciacionCalculo calculos={capitalVivo} titulo={'Capital vivo'}
                                             calcular={calculosFinanciacionCapitalVivo}/>
                        <br/>
                        <FinanciacionCalculo calculos={gastosFiancieros} titulo={'Gastos financieros'}
                                             calcular={calculosFinanciacionGastosFinancieros}/>
                        <br/>
                        <FinanciacionCalculo calculos={devolucionPrestamos} titulo={'Devolución prestamos'}
                                             calcular={calculosFinanciacionDevolucionPrestamos}/>
                    </div>
                    : null
            }
            <br/>
            <Button onClick={guardarFianciacion} variant="outlined" color="primary">
                Guardar avance
            </Button>
        </Container>
    );

}

const mapStateToProps = (state) => {
    return {
        inversiones: state.pveInfo.pveInformacion.inversiones
    }
};

export default connect(mapStateToProps, null)(Financiacion);
