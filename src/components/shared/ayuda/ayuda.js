import React from 'react';

export default function Ayuda() {
    return (
        <div>

            <ul>
                <h3>1. Inversiones: Introducir el costo o valor de las inversiones iniciales, gastos  y las necesarias durante cada año para el funcionamiento adecuado de la idea de negocio o empresa. Gastos iniciales
                   De la inversión inicial son los gastos que vas a hacer una sola vez. Como su nombre lo indica son las cosas indispensables que debes invertir para dar inicio a la idea de negocio.</h3>
                <h3>2. Presupuesto: Introducir la financiación inicial (que ha de ser igual a las inversiones iniciales) y la necesarias para los ejercicios siguientes. Ya sea que te financies con
                 ahorros o tengas necesidad de recurrir a otras fuentes (familia, amigos etc) y determinar a cuánto asciende tu inversión inicial es tan importante como decidir cómo distribuirás el capital.</h3>
                <h3>3. Financiación: Introducir la financiación inicial (que ha de ser igual a las inversiones iniciales) y la necesarias para los ejercicios siguientes. Esta financiación con entidades bancarias generan intereses.</h3>
                <h3>4. Productos: Previsiones de número del unidades de productos o servicios a vender y a comprar, con sus precios ponderados, para calcular las cifras de ventas y aprovisionamiento.</h3>
                <h3>5. Gastos: Gastos de personal, parafiscales, alquileres y servicios e imprevistos.</h3>
                <h3>6. Resultados: Genera la cuenta de resultados como también se introducen los porcentajes de impuestos y beneficios a distribuir según la DIAN.</h3>
            </ul>

        </div>
    )
}
