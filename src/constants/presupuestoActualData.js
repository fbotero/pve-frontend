export let presupuestoM = [
    {
        tiempo: 'Inicio actividad',
        codigoProyecto: '',
        ahorros: 0,
        prestamoFamilia: 0,
        otros: 0,
        recursosPropios: 0,
        premios: 0,
        intercambios: 0,
        cursos: 0,
        financiacionCoste: 0
    },
    {
        tiempo: 'Año 1',
        codigoProyecto: '',
        ahorros: 0,
        prestamoFamilia: 0,
        otros: 0,
        recursosPropios: 0,
        premios: 0,
        intercambios: 0,
        cursos: 0,
        financiacionCoste: 0
    },
    {
        tiempo: 'Año 2',
        codigoProyecto: '',
        ahorros: 0,
        prestamoFamilia: 0,
        otros: 0,
        recursosPropios: 0,
        premios: 0,
        intercambios: 0,
        cursos: 0,
        financiacionCoste: 0
    },
    {
        tiempo: 'Año 3',
        codigoProyecto: '',
        ahorros: 0,
        prestamoFamilia: 0,
        otros: 0,
        recursosPropios: 0,
        premios: 0,
        intercambios: 0,
        cursos: 0,
        financiacionCoste: 0
    },
    {
        tiempo: 'Año 4',
        codigoProyecto: '',
        ahorros: 0,
        prestamoFamilia: 0,
        otros: 0,
        recursosPropios: 0,
        premios: 0,
        intercambios: 0,
        cursos: 0,
        financiacionCoste: 0
    },
    {
        tiempo: 'Año 5',
        codigoProyecto: '',
        ahorros: 0,
        prestamoFamilia: 0,
        otros: 0,
        recursosPropios: 0,
        premios: 0,
        intercambios: 0,
        cursos: 0,
        financiacionCoste: 0
    },
];
