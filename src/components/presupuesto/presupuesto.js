import MaterialTable from "material-table";
import React, {useEffect, useState} from "react";
import {Container} from "@material-ui/core";
import {presupuestoM} from "../../constants/presupuestoActualData";
import {toast} from "react-toastify";
import axios from "axios";
import {constants} from "../../constants/action-types";
import {inversionMoment} from "../../constants/inversionActualData";
import Button from "@material-ui/core/Button";

function Presupuestos(props) {

    const [presupuestos, setPresupuestos] = useState([]);
    let {codigoProyecto} = props;

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    useEffect(() => {
        const getPresupuestos = async () => {
            const response = await axios.get(`${constants.URL_API}/presupuestos/${codigoProyecto}`);
            let data = response.data;
            if(data.length > 0) {
                setState(prevState => {
                    return {...prevState, data}
                });
            }else {
                setState(prevState => {
                    return {...prevState, inversionMoment}
                });
            }

        };
        getPresupuestos();
    }, []);

    const guardarPresupuestos = async () => {
        console.log('Presupuestos a guardar ', presupuestos);
        await axios.post(`${constants.URL_API}/presupuestos/guardar`, presupuestos)
            .then(() => {
                notify("Se ha guardado tu avance");
            }).catch(error => {
                console.log('error: ', error)
            })
    };

    const calcularPresupuestos = (presupuestos) => {
        console.log(presupuestos)
        presupuestos.forEach(value => {
            value.recursosPropios = parseFloat(value.ahorros) + parseFloat(value.prestamoFamilia) + parseFloat(value.otros);
            value.financiacionCoste = parseFloat(value.premios) + parseFloat(value.intercambios) + parseFloat(value.cursos);
            value.codigoProyecto = codigoProyecto
        });
        setPresupuestos(presupuestos)
    };

    const [state, setState] = React.useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Ahorros',
                field: 'ahorros',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo familia',
                field: 'prestamoFamilia',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Otros',
                field: 'otros',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total Recursos Propios',
                field: 'recursosPropios',
                sorting: false,
                type: 'currency',
                editable: 'never',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
            {
                title: 'Premios',
                field: 'premios',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Intercambios',
                field: 'intercambios',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Cursos (Otro)',
                field: 'cursos',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Financiación Coste',
                field: 'financiacionCoste',
                sorting: false,
                type: 'currency',
                editable: 'never',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            }
        ],

        data: presupuestos.length > 0? presupuestos: presupuestoM
    });

    return (
        <Container>
            <MaterialTable
                title="Presupuesto"
                columns={state.columns}
                data={state.data}
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        calcularPresupuestos(data);
                                        return {...prevState, data};
                                    });
                                }
                            }, 600);
                        })
                }}
            />
            <br/>
            <Button onClick={guardarPresupuestos} variant="outlined" color="primary">
                Guardar avance
            </Button>
        </Container>
    );
}

export default Presupuestos
