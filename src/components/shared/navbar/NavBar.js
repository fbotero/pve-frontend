import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import "./NavBar.css"
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import SignedInLinks from "./signInLinks";
import SignedOutLinks from "./singOutLinks";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

function NavBar(props) {

    const classes = useStyles();

    const { auth, profile } = props;

    console.log('Usuario auth ', auth);

    const links = auth.uid ? <SignedInLinks profile={profile} /> : <SignedOutLinks />;

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    Plan Viable de Emprendimiento
                </Typography>
                <Button component={Link} to={'/'} color="inherit">Inicio</Button>
                <Button component={Link} to={'/ayuda'} color="inherit">Ayuda</Button>
                {links}
            </Toolbar>
        </AppBar>
    );
}

const mapDispatchToProps = (state) => {
    return {
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
};

export default connect(mapDispatchToProps)(NavBar);
