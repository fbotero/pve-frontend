import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import {constants} from '../../constants/action-types';

toast.configure();

const initialState = {
    authError: null
};

const notify = (mensaje) => {
    toast(mensaje, {
        position: "bottom-right"
    });
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case constants.LOGIN_ERROR:
            console.log('Loggin error');
            notify('Credenciales incorrectas');
            return {
                ...state,
                authError: 'Login failed'
            };
        case constants.LOGIN_OK:
            console.log('Loggin success');
            notify('Bienvenido !!!');
            return {
                ...state,
                authError: null
            };
        case constants.SIGNOUT_SUCCESS:
            console.log('SignOut success');
            notify('Hasta luego!');
            return state;
        case constants.SIGNUP_SUCCESS:
            console.log('SignUp success');
            return {
                ...state,
                authError: null
            };
        case constants.SIGNUP_ERROR:
            console.log('SignUp failed');

            notify(action.err.message);
            return {
                ...state,
                authError: action.err.message
            };
        default:
            return state
    }
};

export default authReducer;
