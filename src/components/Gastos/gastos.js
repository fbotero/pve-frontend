import React, { useEffect, useState } from 'react';
import MaterialTable from 'material-table';
import { Container } from "@material-ui/core";
import { gastoMoment } from "../../constants/gastosActualData";
import axios from "axios";
import { constants } from "../../constants/action-types";
import Button from "@material-ui/core/Button";
import { toast } from "react-toastify";

const numero2 = 12;
const numero1 = 1; 


function Gastos(props) {

    const [Gastoss, setGastoss] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    let { codigoProyecto } = props;

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    useEffect(() => {
        const getGastos = async () => {
            const response = await axios.get(`${constants.URL_API}/Gastos/${codigoProyecto}`);
            let data = response.data;
            if (data.length > 0) {
                setState(prevState => {
                    return { ...prevState, data }
                });
            } else {
                setState(prevState => {
                    return { ...prevState, gastoMoment }
                });
            }

        };
        getGastos();
    }, []);

    const guardarGastos = async () => {
        console.log('Gastos a guardar ', Gastoss);
        await axios.post(`${constants.URL_API}/Gastos/guardar`, Gastoss)
            .then(() => {
                notify("Se ha guardado tu avance");
            }).catch(error => {
                console.log('error: ', error)
            })
    };

    const hacerCalculos = (Gastos) => {
        Gastos.forEach(value => {
            
            value.salarioMensual2 = parseFloat(value.salarioMensual) * numero2 * parseFloat(value.numeroEmpleadosAno1) ;
            value.alquilerMensual2 = parseFloat(value.alquilerMensual) * numero2 ;
            value.electricidad2 = parseFloat(value.electricidad) * numero2;
            value.telefono2 = parseFloat(value.telefono) * numero2;
            value.materialOficina2 = parseFloat(value.materialOficina) * numero2;
            value.limpieza2 = parseFloat(value.limpieza) * numero2;
            value.totalOtrosGastos = parseFloat(value.electricidad2) + parseFloat(value.telefono2) + parseFloat(value.materialOficina2)+ parseFloat(value.limpieza2);

            
            value.codigoProyecto = codigoProyecto
        });
        setGastoss(Gastos);
    };

    const [state, setState] = useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Salario Medio Mensual',
                field: 'salarioMensual',              
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Salario Medio Mensual Resultado',
                field: 'salarioMensual2',              
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Incremento Salarial Anual',
                field: 'incremenSalarioAnual',
                /* editable: 'never', */
                sorting: false,
                type: 'number',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'N° De Empleados Año 1',
                field: 'numeroEmpleadosAno1',
                /*  editable: 'never', */
                sorting: false,
                type: 'number',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'N° De Empleados Año 2',
                field: 'numeroEmpleadosAno2',
                /* editable: 'never', */
                sorting: false,
                type: 'number',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'N° De Empleados Año 3',
                field: 'numeroEmpleadosAno3',
                /* editable: 'never', */
                sorting: false,
                type: 'number',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'N° De Empleados Año 4',
                field: 'numeroEmpleadosAno4',
                /* editable: 'never', */
                sorting: false,
                type: 'number',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'N° De Empleados Año 5',
                field: 'numeroEmpleadosAno5',
                /* editable: 'never', */
                sorting: false,
                type: 'number',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: '% coste Seguridad Social',
                field: 'porcentajeSeguSocial',
                /* editable: 'never', */
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Total gastos de personal',
                field: 'totalGastosPersonal',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center', backgroundColor:'yellow', color: 'black' }
            },

            {
                title: '------',
            },
            {
                title: 'Alquiler Mensual',
                field: 'alquilerMensual',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Alquiler Mensual Resultado',
                field: 'alquilerMensual2',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Subida Anual Prevista En %',
                field: 'SubidaAnual',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: '------',
            },
            {
                title: 'Electricidad',
                field: 'electricidad',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Electricidad resultado ',
                field: 'electricidad2',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Telefono',
                field: 'telefono',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Telefono resultado',
                field: 'telefono2',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Material De Oficina',
                field: 'materialOficina',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Material De Oficina resultado',
                field: 'materialOficina2',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Limpieza',
                field: 'limpieza',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Limpieza resultado',
                field: 'limpieza2',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Seguros',
                field: 'seguros',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Otros',
                field: 'otros',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: '------',
            },
            {
                title: 'Subida Media Anual En %',
                field: 'subidaMedia',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
            {
                title: 'Total Otros Gastos',
                field: 'totalOtrosGastos',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center',backgroundColor:'yellow', color: 'black' }
            },
            {
                title: 'TOTAL GASTOS',
                field: 'totalGastos',
                sorting: false,
                type: 'currency',
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center',backgroundColor:'yellow', color: 'black' }
            },

        ],
        data: Gastoss.length > 0 ? Gastoss : gastoMoment
    });

    console.log('dabt', Gastoss);

    return (
        <Container>
            <MaterialTable
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                title="Gastos"
                columns={state.columns}
                data={state.data}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        hacerCalculos(data);
                                        return { ...prevState, data };
                                    });
                                }
                            }, 600);
                        }),
                }}
            />
            <br />
            <Button onClick={guardarGastos} variant="outlined" color="primary">
                Guardar Avance
            </Button>
        </Container>
    );
}

export default Gastos
