// Tabla para adicionar los campos necesarios para realizar el paso de financiacion
import React, {useEffect, useState} from "react";
import {financiacionCalculoMoment} from "../../constants/financiacionCalculoActual";
import {Container} from "@material-ui/core";
import MaterialTable from "material-table";

export const FinanciacionCalculo = (props) => {

    let {calculos, titulo, calcular} = props;

    const [state, setState] = useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo año 0',
                field: 'prestamo0',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo año 1',
                field: 'prestamo1',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo año 2',
                field: 'prestamo2',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo año 3',
                field: 'prestamo3',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo año 4',
                field: 'prestamo4',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Prestamo año 5',
                field: 'prestamo5',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total prestamo',
                field: 'totalPrestamo',
                sorting: false,
                type: 'currency',
                editable: 'never',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            }
        ],
        data: calculos.length > 0? calculos : financiacionCalculoMoment
    });

    return (
        <Container>
            <MaterialTable
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                title={titulo}
                columns={state.columns}
                data={state.data}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        calcular(data);
                                        return {...prevState, data};
                                    });
                                }
                            }, 600);
                        }),
                }}
            />
        </Container>
    )

};
