export class Inversion {

    infraMaquinas: Number;
    mobiliario: Number;
    equiposInformaticos: Number;
    vehiculos: Number;
    otros: Number;
    totalNoCorriente: Number;
    existenciasIniciales: Number;
    liquidez: Number;
    totalCorriente: Number;
    totalInversion: Number;

    constructor(infraMaquinas: Number, mobiliario: Number, equiposInformaticos: Number, vehiculos: Number, otros: Number, totalNoCorriente: Number, existenciasIniciales: Number, liquidez: Number, totalCorriente: Number, totalInversion: Number) {
        this.infraMaquinas = infraMaquinas;
        this.mobiliario = mobiliario;
        this.equiposInformaticos = equiposInformaticos;
        this.vehiculos = vehiculos;
        this.otros = otros;
        this.totalNoCorriente = totalNoCorriente;
        this.existenciasIniciales = existenciasIniciales;
        this.liquidez = liquidez;
        this.totalCorriente = totalNoCorriente;
        this.totalInversion = totalInversion;
    }


}
