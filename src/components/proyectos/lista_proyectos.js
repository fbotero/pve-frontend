import React, {useEffect, useState} from "react";
import {constants} from "../../constants/action-types";
import axios from "axios";
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Link, Redirect} from 'react-router-dom'
import {connect} from "react-redux";
import {toast} from "react-toastify";

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%',
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

const notify = (mensaje) => {
    toast(mensaje, {
        position: "bottom-right"
    });
};


const ListaProyectos = (props) => {
    const classes = useStyles();
    const [isLoading, setIsLoading] = useState(false);
    const [proyectos, setProyectos] = useState([]);

    useEffect(() => {
        setIsLoading(true);
        const getData = async () => {
            await axios.get(`${constants.URL_API}/proyectos/${props.auth.uid}`)
                .then(response => {
                    setProyectos(response.data);
                })
                .catch(error => {
                    notify('Error consultando los proyectos')
                });
        };
        getData();
    }, []);

    console.log('Proyectos ', proyectos);

    if (!props.auth.uid) return <Redirect to={'/login'}/>;

    return (
        <main>
            <div className={classes.heroContent}>
                <Container maxWidth="sm">
                    <Typography component="h1" variant="h3" align="center" color="textPrimary" gutterBottom>
                        Mis proyectos
                    </Typography>
                    {
                        proyectos.length > 0 ? <Typography variant="h5" align="center" color="textSecondary" paragraph>
                                Aqui encontrarás los proyectos que has creado, selecciona ver plan viable para conocer tu
                                avance.
                            </Typography> :
                            <Typography variant="h5" align="center" color="textSecondary" paragraph>
                                Aun no tienes proyectos creados.
                            </Typography>
                    }
                    <div className={classes.heroButtons}>
                        <Grid container spacing={2} justify="center">
                            <Grid item>
                                <Button component={Link} to={'/crear-proyecto'} variant="outlined" color="primary">
                                    Crear proyecto
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </div>
            <Container className={classes.cardGrid} maxWidth="md">
                <Grid container spacing={4}>
                    {proyectos.map((proyecto) => (
                        <Grid item key={proyecto.id} xs={12} sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image={proyecto.logo}
                                    title="logo"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {proyecto.nombre}
                                    </Typography>
                                    <Typography>
                                        {proyecto.descripcion}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button component={Link} to={`/crear-plan/${proyecto.codigoProyecto}`} size="small"
                                            color="primary">
                                        Ver plan viable
                                    </Button>
                                    <Button component={Link} to={`/editar-proyecto/${proyecto.codigoProyecto}`}
                                            size="small" color="primary">
                                        Editar
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </main>
    );
};

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
};

export default connect(mapStateToProps, null)(ListaProyectos)
