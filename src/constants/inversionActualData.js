export let inversionMoment = [
    {
        tiempo: 'Inicio actividad',
        codigoProyecto: '',
        infraMaquinas: 0,
        mobiliario: 0,
        vehiculos: 0,
        otros: 0,
        equiposInformaticos: 0,
        totalNoCorriente: 0,
        existenciasIniciales: 0,
        liquidez: 0,
        totalCorriente: 0,
        totalInversion: 0
    },
    {
        tiempo: 'Año 1',
        codigoProyecto: '',
        infraMaquinas: 0,
        mobiliario: 0,
        vehiculos: 0,
        otros: 0,
        equiposInformaticos: 0,
        totalNoCorriente: 0,
        existenciasIniciales: 0,
        liquidez: 0,
        totalCorriente: 0,
        totalInversion: 0
    },
    {
        tiempo: 'Año 2',
        codigoProyecto: '',
        infraMaquinas: 0,
        mobiliario: 0,
        vehiculos: 0,
        otros: 0,
        equiposInformaticos: 0,
        totalNoCorriente: 0,
        existenciasIniciales: 0,
        liquidez: 0,
        totalCorriente: 0,
        totalInversion: 0
    },
    {
        tiempo: 'Año 3',
        codigoProyecto: '',
        infraMaquinas: 0,
        mobiliario: 0,
        vehiculos: 0,
        otros: 0,
        equiposInformaticos: 0,
        totalNoCorriente: 0,
        existenciasIniciales: 0,
        liquidez: 0,
        totalCorriente: 0,
        totalInversion: 0
    },
    {
        tiempo: 'Año 4',
        codigoProyecto: '',
        infraMaquinas: 0,
        mobiliario: 0,
        vehiculos: 0,
        otros: 0,
        equiposInformaticos: 0,
        totalNoCorriente: 0,
        existenciasIniciales: 0,
        liquidez: 0,
        totalCorriente: 0,
        totalInversion: 0
    },
    {
        tiempo: 'Año 5',
        codigoProyecto: '',
        infraMaquinas: 0,
        mobiliario: 0,
        vehiculos: 0,
        otros: 0,
        equiposInformaticos: 0,
        totalNoCorriente: 0,
        existenciasIniciales: 0,
        liquidez: 0,
        totalCorriente: 0,
        totalInversion: 0
    },
];
