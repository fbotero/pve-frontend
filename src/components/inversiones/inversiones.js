import React, {useEffect, useState} from 'react';
import MaterialTable from 'material-table';
import {Container, IconButton, makeStyles} from "@material-ui/core";
import {inversionMoment} from "../../constants/inversionActualData";
import axios from "axios";
import {constants} from "../../constants/action-types";
import Button from "@material-ui/core/Button";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import {toast} from "react-toastify";
import {connect} from "react-redux";
import {inversionesEstado} from '../../actions/pveActions'
import CalculosIntermedios from '../calculos_intermedios/calculos_intermedios';

    
const useStyles = makeStyles((theme) => ({
    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

function Inversiones(props) {
    
    const classes = useStyles();
    const mensajeCalculos1 = 'Ver cálculos Intermedios';
    const mensajeCalculos2 = 'Ocultar cálculos Intermedios';

    const [inversions, setInversions] = useState([]);
    const [ isLoading, setIsLoading] = useState(false);
    let [verCalculos, setVerCalculos] = useState(false);

    let {codigoProyecto} = props;

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    useEffect(() => {
        const getInversiones = async () => {
            const response = await axios.get(`${constants.URL_API}/inversiones/${codigoProyecto}`);
            let data = response.data;
            if(data.length > 0) {
                setState(prevState => {
                    return {...prevState, data}
                });
                props.inversionesEstadoI(data);
            }else {
                setState(prevState => {
                    return {...prevState, inversionMoment}
                });
            }

        };
        getInversiones();
    }, []);

    const guardarInversiones = async () => {
        console.log('Inversiones a guardar ', inversions);
        await axios.post(`${constants.URL_API}/inversiones/guardar`, inversions)
            .then(() => {
                notify("Se ha guardado tu avance");
            }).catch(error => {
                console.log('error: ', error)
            })
    };

    const hacerCalculos = (inversiones) => {
        inversiones.forEach(value => {
            value.totalNoCorriente = parseFloat(value.infraMaquinas) + parseFloat(value.mobiliario) + parseFloat(value.vehiculos) + parseFloat(value.otros) + parseFloat(value.equiposInformaticos);
            value.totalCorriente = parseFloat(value.existenciasIniciales) + parseFloat(value.liquidez);
            value.totalInversion = parseFloat(value.totalNoCorriente) + parseFloat(value.totalCorriente);
            value.codigoProyecto = codigoProyecto
        });
        setInversions(inversiones);
        props.inversionesEstadoI(inversiones);
    };

    const [state, setState] = useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Infraestructura',
                field: 'infraMaquinas',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Mobiliario',
                field: 'mobiliario',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Equipos informáticos',
                field: 'equiposInformaticos',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Vehículos',
                field: 'vehiculos',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Otros',
                field: 'otros',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total no corriente',
                field: 'totalNoCorriente',
                editable: 'never',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
            {
                title: 'Existencias iniciales',
                field: 'existenciasIniciales',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Liquidez & Tesorería',
                field: 'liquidez',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center'}
            },
            {
                title: 'Total corriente',
                field: 'totalCorriente',
                editable: 'never',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            },
            {
                title: 'Total inversion',
                field: 'totalInversion',
                editable: 'never',
                sorting: false,
                type: 'currency',
                cellStyle: {textAlign: 'center'},
                headerStyle: {textAlign: 'center', backgroundColor: 'yellow', color: 'black'}
            }
        ],
        data: inversions.length > 0? inversions: inversionMoment
    });

    console.log('dabt', inversions);

    function verOcultarCalculos() {
        setVerCalculos(!verCalculos);
    }

    return (
        <Container>
            <MaterialTable
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                title="Inversiones"
                columns={state.columns}
                data={state.data}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        hacerCalculos(data);
                                        return {...prevState, data};
                                    });
                                }
                            }, 600);
                        }),
                }}
            />
             <br/>
            <div align={'center'} style={{paddingTop: 10}}>
                <IconButton onClick={verOcultarCalculos} aria-label="delete" className={classes.margin} size="small">
                    {!verCalculos ? mensajeCalculos1 : mensajeCalculos2} {!verCalculos ?
                    <ArrowDownwardIcon fontSize="inherit"/> : <ArrowUpwardIcon fontSize="inherit"/>}
                </IconButton>
            </div>
            <br/>
            {
                verCalculos ?
                    <div>
                        <CalculosIntermedios />
                    </div>
                    : null
            }
            <br/>
            <Button onClick={guardarInversiones} variant="outlined" color="primary">
                Guardar avance
            </Button>
        </Container>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        inversionesEstadoI: (inversiones) => dispatch(inversionesEstado(inversiones))
    }
};

export default connect(null, mapDispatchToProps)(Inversiones)
