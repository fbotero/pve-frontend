import pveReducer from "./pveReducer";
import {combineReducers} from "redux";
import { firebaseReducer } from 'react-redux-firebase'
import authReducer from "./auth/authReducer";
import {firestoreReducer} from "redux-firestore";

const rootReducer = combineReducers({
    auth: authReducer,
    pveInfo: pveReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer
});

export default rootReducer;
