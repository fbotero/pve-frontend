import {constants} from '../constants/action-types';

const state = {
    pveInformacion: {
        id_usuario: '',
        inversiones: []
    }
};


function agregarInversiones(miState, data) {
    console.log('lllll ', data);
    return {
        ...miState, pveInformacion: {
            id_usuario: miState.pveInformacion.id_usuario,
            inversiones: data
        }
    };
}

function pveReducer(miState = state, action) {
    switch (action.type) {
        case constants.RESET_STATE:
            return {
                ...state
            };
        case constants.ADD_USER_DATA:
            return agregarInformacionUsuario(miState, action.data);
        case constants.ADD_INVERSION_DATA:
            return agregarInversiones(miState, action.data);
        default:
            return miState;
    }

}

function agregarInformacionUsuario(miState, uid) {
    return {
        ...miState, pveInformacion: {
            id_usuario: uid,
            inversiones: miState.pveInformacion.inversiones
        }
    };
}

export default pveReducer;
