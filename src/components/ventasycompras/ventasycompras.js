import React, { useEffect, useState } from 'react';
import MaterialTable from 'material-table';
import { Container } from "@material-ui/core";
import { ventaycompraMoment } from "../../constants/ventasycomprasActualData";
import axios from "axios";
import { constants } from "../../constants/action-types";
import Button from "@material-ui/core/Button";
import { toast } from "react-toastify";

const numero2 = 12;
const numero1 = 1; 


function VentasyCompras(props) {

    const [VentasyComprass, setVentasyComprass] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    let { codigoProyecto } = props;

    const notify = (mensaje) => {
        toast(mensaje, {
            position: "bottom-right"
        });
    };

    useEffect(() => {
        const getVentasyCompras = async () => {
            const response = await axios.get(`${constants.URL_API}/VentasyCompras/${codigoProyecto}`);
            let data = response.data;
            if (data.length > 0) {
                setState(prevState => {
                    return { ...prevState, data }
                });
            } else {
                setState(prevState => {
                    return { ...prevState, ventaycompraMoment }
                });
            }

        };
        getVentasyCompras();
    }, []);

    const guardarVentasyCompras = async () => {
        console.log('VentasyCompras a guardar ', VentasyComprass);
        await axios.post(`${constants.URL_API}/VentasyCompras/guardar`, VentasyComprass)
            .then(() => {
                notify("Se ha guardado tu avance");
            }).catch(error => {
                console.log('error: ', error)
            })
    };

    const hacerCalculos = (VentasyCompras) => {
        VentasyCompras.forEach(value => {
            
            value.salarioMensual2 = parseFloat(value.salarioMensual) * numero2 * parseFloat(value.numeroEmpleadosAno1) ;
            value.alquilerMensual2 = parseFloat(value.alquilerMensual) * numero2 ;
            value.electricidad2 = parseFloat(value.electricidad) * numero2;
            value.telefono2 = parseFloat(value.telefono) * numero2;
            value.materialOficina2 = parseFloat(value.materialOficina) * numero2;
            value.limpieza2 = parseFloat(value.limpieza) * numero2;
            value.totalOtrosVentasyCompras = parseFloat(value.electricidad2) + parseFloat(value.telefono2) + parseFloat(value.materialOficina2)+ parseFloat(value.limpieza2);

            
            value.codigoProyecto = codigoProyecto
        });
        setVentasyComprass(VentasyCompras);
    };

    const [state, setState] = useState({
        columns: [
            {
                title: 'Tiempo',
                field: 'tiempo',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio1:Unidades',
                field: 'unidades',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio1:precios',
                field: 'precio',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio1:Ingresos',
                field: 'ingresos',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            ////////////
            {
                title: 'Producto/Servicio2:Unidades',
                field: 'unidades',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio2:precios',
                field: 'precio',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio2:Ingresos',
                field: 'ingresos',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            /////////

            {
                title: 'Producto/Servicio3:Unidades',
                field: 'unidades',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio3:precios',
                field: 'precio',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio3:Ingresos',
                field: 'ingresos',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            ////////////
            {
                title: 'Producto/Servicio4:Unidades',
                field: 'unidades',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio4:precios',
                field: 'precio',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio4:Ingresos',
                field: 'ingresos',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            ///////////
            {
                title: 'Producto/Servicio5:Unidades',
                field: 'unidades',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio5:precios',
                field: 'precio',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },

            {
                title: 'Producto/Servicio5:Ingresos',
                field: 'ingresos',
                editable: 'never',
                sorting: false,
                removable: false,
                cellStyle: { textAlign: 'center' },
                headerStyle: { textAlign: 'center' }
            },
        ],
        data: VentasyComprass.length > 0 ? VentasyComprass : ventaycompraMoment
    });

    console.log('dabt', VentasyComprass);

    return (
        <Container>
            <MaterialTable
                options={{
                    paging: false,
                    search: false,
                    draggable: false,
                    rowStyle: {
                        textAlign: 'center',
                        background: '#ddd'
                    },
                    headerStyle: {
                        backgroundColor: '#3f51b5',
                        color: '#FFF'
                    }
                }}
                title="Ventas/Ingresos"
                columns={state.columns}
                data={state.data}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        data[data.indexOf(oldData)] = newData;
                                        hacerCalculos(data);
                                        return { ...prevState, data };
                                    });
                                }
                            }, 600);
                        }),
                }}
            />
            <br />
            <Button onClick={guardarVentasyCompras} variant="outlined" color="primary">
                Guardar Avance
            </Button>
        </Container>
    );
}

export default VentasyCompras