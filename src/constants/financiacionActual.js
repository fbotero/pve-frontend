export let financiacionMoment = [
    {
        tiempo: 'Inicio actividad',
        codigoProyecto: '',
        pptoEmprendedor: 0,
        prestamos: 0,
        tipoInteres: 0,
        anios: 0,
        totalFinaciacion: 0,
        inversion: 0,
        financiacion: 0
    },
    {
        tiempo: 'Año 1',
        codigoProyecto: '',
        pptoEmprendedor: 0,
        prestamos: 0,
        tipoInteres: 0,
        anios: 0,
        totalFinaciacion: 0,
        inversion: 0,
        financiacion: 0
    },
    {
        tiempo: 'Año 2',
        codigoProyecto: '',
        pptoEmprendedor: 0,
        prestamos: 0,
        tipoInteres: 0,
        anios: 0,
        totalFinaciacion: 0,
        inversion: 0,
        financiacion: 0
    },
    {
        tiempo: 'Año 3',
        codigoProyecto: '',
        pptoEmprendedor: 0,
        prestamos: 0,
        tipoInteres: 0,
        anios: 0,
        totalFinaciacion: 0,
        inversion: 0,
        financiacion: 0
    },
    {
        tiempo: 'Año 4',
        codigoProyecto: '',
        pptoEmprendedor: 0,
        prestamos: 0,
        tipoInteres: 0,
        anios: 0,
        totalFinaciacion: 0,
        inversion: 0,
        financiacion: 0
    },
    {
        tiempo: 'Año 5',
        codigoProyecto: '',
        pptoEmprendedor: 0,
        prestamos: 0,
        tipoInteres: 0,
        anios: 0,
        totalFinaciacion: 0,
        inversion: 0,
        financiacion: 0
    },
];
